# OpenSatCom - Signal generation from raw IQ samples

First of all, please make sure to read the project report to better understand the goals, shortcomings and so on.

The code has been developed for the Nucleo-L476RG with [this custom shield](https://gitlab.com/scrobotics/opensatcom/lsf-opensatcom-hw) in mind. The peripherals listed in the project report are also necessary.

This repository uses [PlatformIO](https://docs.platformio.org/). We recommend using it along with its VS Code Extension to take full advantage of all its features (like debugging), but if you'd like to use it from the CLI, these are the two most important commands:

```
pio run --environment nucleo_l476rg                           # Build
pio run --target upload --environment nucleo_l476rg           # Flash
```

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

    void sd_card_open(const char *filename);
    void sd_card_close();
    int sd_card_read(char *buffer, int len);
    int sd_card_size();

#ifdef __cplusplus
}
#endif

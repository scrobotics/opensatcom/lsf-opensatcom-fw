#include "stm32l4xx_hal.h"
#include <assert.h>

#include <drivers/spi.h>
#include <drivers/dac.h>
#include <drivers/timer.h>
#include <sd_card.h>

// #define TEST_DMA
#define MAX_SIGNAL_LEN 20000 // Maximum number of bytes reserved for the signal

static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};

  /* MSI is enabled after System reset, activate PLL with MSI as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.MSICalibrationValue = RCC_MSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLP = 7;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    /* Initialization Error */
    while (1)
      ;
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    /* Initialization Error */
    while (1)
      ;
  }
}

int main(void)
{
  HAL_Init();
  SystemClock_Config();
  spi_init();
  dac_init();
  // timer_init(2);  // NOT USED ANYMORE

#ifdef TEST_DMA
  // Generate test output:
  // I: ramp from 0 to 255
  // Q: ramp from 255 to 0
  static uint8_t buffer_i[256] = {0};
  static uint8_t buffer_q[256] = {0};
  assert(sizeof(buffer_i) == sizeof(buffer_q));
  for (int i = 0; i < 256; i++)
  {
    buffer_i[i] = i;
    buffer_q[i] = 256 - i - 1;
  }

  dac_dma_start(buffer_i, buffer_q, 256);
  while (1)
  {
  }
#else
  // Read I samples
  static uint8_t signal_i[MAX_SIGNAL_LEN];
  sd_card_open("signal_i.bin");
  int size_i = sd_card_size();
  assert(sd_card_read((char *)signal_i, size_i) == size_i);
  sd_card_close();

  // Read Q samples
  static uint8_t signal_q[MAX_SIGNAL_LEN];
  sd_card_open("signal_q.bin");
  int size_q = sd_card_size();
  assert(sd_card_read((char *)signal_q, size_q) == size_q);
  sd_card_close();

  assert(size_i == size_q);
  timer_init(4);
  dac_dma_start(signal_i, signal_q, size_i);

  while (1)
  {
  }
#endif
}

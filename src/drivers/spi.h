#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

    void spi_init();
    void spi_test();

#ifdef __cplusplus
}
#endif

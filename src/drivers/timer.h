#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

    void timer_init(uint8_t instance);
    void timer_delay_us(uint32_t delay);

#ifdef __cplusplus
}
#endif

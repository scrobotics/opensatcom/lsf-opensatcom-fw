#include "dac.h"

#include "stm32l4xx_hal.h"

/**
 * Configuration of pins and clocks.
 */
#define DACx DAC1
#define DACx_CLK_ENABLE() __HAL_RCC_DAC1_CLK_ENABLE()
#define DACx_CLK_DISABLE() __HAL_RCC_DAC1_CLK_DISABLE()
#define DACx_CH1_GPIO_CLK_ENABLE() __HAL_RCC_GPIOA_CLK_ENABLE()
#define DACx_CH2_GPIO_CLK_ENABLE() __HAL_RCC_GPIOA_CLK_ENABLE()

#define DACx_CH1_PIN GPIO_PIN_4
#define DACx_CH1_GPIO_PORT GPIOA
#define DACx_CH2_PIN GPIO_PIN_5
#define DACx_CH2_GPIO_PORT GPIOA

/**
 *  Configuration of DMA channels.
 */
#define DACxCH1_DMA_INSTANCE DMA1_Channel3
#define DACxCH1_DMA_IRQn DMA1_Channel3_IRQn
#define DACxCH1_DMA_IRQHandler DMA1_Channel3_IRQHandler
#define DACxCH2_DMA_INSTANCE DMA1_Channel4
#define DACxCH2_DMA_IRQn DMA1_Channel4_IRQn
#define DACxCH2_DMA_IRQHandler DMA1_Channel4_IRQHandler
#define DMAx_CLK_ENABLE() __HAL_RCC_DMA1_CLK_ENABLE()
#define DMAx_CLK_DISABLE() __HAL_RCC_DMA1_CLK_DISABLE()

DAC_HandleTypeDef hdac;
DMA_HandleTypeDef hdma_dac1;
DMA_HandleTypeDef hdma_dac2;

void HAL_DAC_MspInit(DAC_HandleTypeDef *dacHandle)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    if (dacHandle->Instance == DACx)
    {
        DACx_CLK_ENABLE();
        DMAx_CLK_ENABLE();
        DACx_CH1_GPIO_CLK_ENABLE();
        DACx_CH2_GPIO_CLK_ENABLE();

        GPIO_InitStruct.Pin = DACx_CH1_PIN;
        GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(DACx_CH1_GPIO_PORT, &GPIO_InitStruct);

        GPIO_InitStruct.Pin = DACx_CH2_PIN;
        HAL_GPIO_Init(DACx_CH2_GPIO_PORT, &GPIO_InitStruct);

        // Configure DAC channel 1
        hdma_dac1.Instance = DACxCH1_DMA_INSTANCE;
        hdma_dac1.Init.Request = DMA_REQUEST_6;
        hdma_dac1.Init.Direction = DMA_MEMORY_TO_PERIPH;
        hdma_dac1.Init.PeriphInc = DMA_PINC_DISABLE;
        hdma_dac1.Init.MemInc = DMA_MINC_ENABLE;
        hdma_dac1.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_dac1.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
        hdma_dac1.Init.Mode = DMA_CIRCULAR;
        hdma_dac1.Init.Priority = DMA_PRIORITY_HIGH;

        HAL_DMA_Init(&hdma_dac1);

        /* Associate the initialized DMA handle to the DAC handle */
        __HAL_LINKDMA(&hdac, DMA_Handle1, hdma_dac1);

        // Configure DAC channel 2
        hdma_dac2.Instance = DACxCH2_DMA_INSTANCE;
        hdma_dac2.Init.Request = DMA_REQUEST_5;
        hdma_dac2.Init.Direction = DMA_MEMORY_TO_PERIPH;
        hdma_dac2.Init.PeriphInc = DMA_PINC_DISABLE;
        hdma_dac2.Init.MemInc = DMA_MINC_ENABLE;
        hdma_dac2.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_dac2.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
        hdma_dac2.Init.Mode = DMA_CIRCULAR;
        hdma_dac2.Init.Priority = DMA_PRIORITY_HIGH;

        HAL_DMA_Init(&hdma_dac2);

        /* Associate the initialized DMA handle to the DAC handle */
        __HAL_LINKDMA(&hdac, DMA_Handle2, hdma_dac2);

        /* Enable the DMA Channels IRQ */
        HAL_NVIC_SetPriority(DACxCH1_DMA_IRQn, 2, 0);
        HAL_NVIC_EnableIRQ(DACxCH1_DMA_IRQn);
        HAL_NVIC_SetPriority(DACxCH2_DMA_IRQn, 3, 0);
        HAL_NVIC_EnableIRQ(DACxCH2_DMA_IRQn);
    }
}

void HAL_DAC_MspDeInit(DAC_HandleTypeDef *dacHandle)
{
    if (dacHandle->Instance == DACx)
    {
        DACx_CLK_DISABLE();
        DMAx_CLK_DISABLE();
        HAL_GPIO_DeInit(DACx_CH1_GPIO_PORT, DACx_CH1_PIN);
        HAL_GPIO_DeInit(DACx_CH2_GPIO_PORT, DACx_CH2_PIN);

        HAL_DMA_DeInit(dacHandle->DMA_Handle1);
        HAL_DMA_DeInit(dacHandle->DMA_Handle2);
        HAL_NVIC_DisableIRQ(DACxCH1_DMA_IRQn);
        HAL_NVIC_DisableIRQ(DACxCH2_DMA_IRQn);
    }
}

void dac_init(void)
{
    DAC_ChannelConfTypeDef sConfig = {0};

    hdac.Instance = DACx;
    HAL_DAC_Init(&hdac);

    sConfig.DAC_SampleAndHold = DAC_SAMPLEANDHOLD_DISABLE;
    sConfig.DAC_Trigger = DAC_TRIGGER_T4_TRGO;
    sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
    sConfig.DAC_ConnectOnChipPeripheral = DAC_CHIPCONNECT_ENABLE;
    sConfig.DAC_UserTrimming = DAC_TRIMMING_FACTORY;
    HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_1);
    HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_2);

    // HAL_DAC_Start(&hdac, DAC_CHANNEL_1);
    // HAL_DAC_Start(&hdac, DAC_CHANNEL_2);
}

bool dac_is_ready(void)
{
    return HAL_DAC_GetState(&hdac) == HAL_DAC_STATE_READY;
}

void dac_write(uint16_t x_8b, uint16_t y_8b)
{
    static uint8_t dac_modifier_amp_x = 15;
    static uint8_t dac_modifier_off_x = 150;
    static uint8_t dac_modifier_amp_y = 15;
    static uint8_t dac_modifier_off_y = 150;

    /**
     * Full range is 12 bits (0 to 4095). The modifiers adjust both the 
     * amplitude and the offset (the zero). This is done because the DAC
     * could be non linear in part of the range (usually in the lower or
     * upper limits). The element in the output might have specific
     * requirements too.
     */
    uint16_t x_12b = x_8b * dac_modifier_amp_x + dac_modifier_off_x;
    uint16_t y_12b = y_8b * dac_modifier_amp_y + dac_modifier_off_y;

    if (hdac.State == HAL_DAC_STATE_RESET)
    {
        dac_init();
    }
    HAL_DACEx_DualSetValue(&hdac, DAC_ALIGN_12B_R, x_12b, y_12b);
}

void dac_dma_start(uint8_t *buffer_i, uint8_t *buffer_q, uint32_t len)
{
    HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t *)buffer_i, len, DAC_ALIGN_8B_R);
    HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_2, (uint32_t *)buffer_q, len, DAC_ALIGN_8B_R);
}

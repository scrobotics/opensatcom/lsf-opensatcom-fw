#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

    void led_init();
    void led_toggle();

#ifdef __cplusplus
}
#endif

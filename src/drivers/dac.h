#pragma once

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

    void dac_init(void);
    bool dac_is_ready(void);
    void dac_write(uint16_t x_8b, uint16_t y_8b);
    void dac_dma_start(uint8_t *buffer_i, uint8_t *buffer_q, uint32_t len);

#ifdef __cplusplus
}
#endif

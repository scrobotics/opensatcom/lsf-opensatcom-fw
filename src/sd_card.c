#include "sd_card.h"

#include <fs/fatfs_sd.h>
#include <fs/user_diskio.h>
#include <ff.h>

static FATFS fs;
static FIL fil;
static char USERPath[4]; /* USER logical drive path */
static FSIZE_t fsize;
static FSIZE_t nextbyte;

void sd_card_open(const char *filename)
{
    FATFS_LinkDriver(&USER_Driver, USERPath);

    /* Wait for SD module reset */
    HAL_Delay(500);

    /* Mount SD Card */
    if (f_mount(&fs, "", 0) != FR_OK)
        while (1)
            ;

    /* Open file to write */
    if (f_open(&fil, filename, FA_READ) != FR_OK)
        while (1)
            ;

    fsize = f_size(&fil);
    nextbyte = 0;
}

void sd_card_close()
{
    /* Close file */
    if (f_close(&fil) != FR_OK)
        while (1)
            ;

    /* Unmount SDCARD */
    if (f_mount(NULL, "", 1) != FR_OK)
        while (1)
            ;
}

int sd_card_size()
{
    return fil.obj.objsize;
}

int sd_card_read(char *buffer, int len)
{
    UINT read;

    // Read (it also makes sure we don't read more bytes than available)
    if (f_read(&fil, buffer, len, &read) != FR_OK)
    {
        return -1;
    }

    // Seek
    nextbyte += len;
    // If file over go back to the beginning
    if (nextbyte >= fsize)
    {
        nextbyte = 0;
    }
    if (f_lseek(&fil, nextbyte) != FR_OK)
    {
        return -1;
    }

    return read;
}

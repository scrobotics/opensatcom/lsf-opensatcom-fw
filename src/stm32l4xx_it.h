#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

  void NMI_Handler(void);
  void HardFault_Handler(void);
  void MemManage_Handler(void);
  void BusFault_Handler(void);
  void UsageFault_Handler(void);
  void SVC_Handler(void);
  void DebugMon_Handler(void);
  void PendSV_Handler(void);
  void SysTick_Handler(void);
  void SPIx_IRQHandler(void);
  void TIM4_IRQHandler(void);
  void DMA1_Channel3_IRQHandler(void);
  void DMA1_Channel4_IRQHandler(void);

#ifdef __cplusplus
}
#endif
